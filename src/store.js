import Vue from "vue";
import Vuex from "vuex";
import productsApi from "./api/productsApi";
import clientsApi from "./api/clientsApi";
import storesApi from "./api/storesApi";

Vue.use(Vuex);

const READ_PRODUCTS_SUCCESSFUL = "READ_PRODUCTS_SUCCESSFUL";
const READ_CLIENTS_SUCCESSFUL = "READ_CLIENTS_SUCCESSFUL";
const READ_STORES_SUCCESSFUL = "READ_STORES_SUCCESSFUL";
//const READ_MOISTURE_VALUES_FAILED = 'READ_MOISTURE_VALUES_FAILED'

const state = {
  products: [],
  clients: [],
  stores: []
};

const mutations = {
  [READ_PRODUCTS_SUCCESSFUL](state, data) {
    state.products = data;
  },

  [READ_CLIENTS_SUCCESSFUL](state, data) {
    state.clients = data;
  },

  [READ_STORES_SUCCESSFUL](state, data) {
    state.stores = data;
  },
};

const actions = {
  getProducts({ commit }) {
    productsApi.getProducts().then((response) => {
      commit(READ_PRODUCTS_SUCCESSFUL, response.data);
    });
  },

  getClients({ commit }) {
    clientsApi.getClients().then((response) => {
      commit(READ_CLIENTS_SUCCESSFUL, response.data);
    });
  },

  getStores({ commit }) {
    storesApi.getStores().then((response) => {
      commit(READ_STORES_SUCCESSFUL, response.data);
    });
  },
};

const getters = {
  products() {
    return state.products;
  },

  clients() {
    return state.clients;
  },

  stores() {
    return state.stores;
  },
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
});
