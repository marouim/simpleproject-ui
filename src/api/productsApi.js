import axios from "axios";

const api = {
  baseUrl: "/api",
  productsUrl: "/products",

  getProducts() {
    return axios({
      method: "GET",
      baseURL: this.baseUrl,
      url: this.productsUrl,
    });
  },
};

export default api;
