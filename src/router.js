import Vue from "vue";
import VueRouter from "vue-router";

// Import router components
import Products from "./components/Products.vue";
import Clients from "./components/Clients.vue";
import Stores from "./components/Stores.vue";

Vue.use(VueRouter);

const routes = [
  {
    name: "products",
    path: "/",
    component: Products,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "clients",
    path: "/",
    component: Clients,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "stores",
    path: "/",
    component: Stores,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
